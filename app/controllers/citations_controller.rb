class CitationsController < ApplicationController

  before_action :find_citation, only: [:show,:edit,:update]
  def index
    @citations = Citation.all
  end

  def new
    @citation = Citation.new
  end

  def create
    @citation = Citation.new citation_params
    if @citation.save
      redirect_to action: "index"
    else
      render 'new'
    end
  end
  def show

  end

  private
  def citation_params
    params.require(:citation).permit(:title, :content)
  end

  def find_citation
    @citation = citation.find(params[:id])
  end

end
